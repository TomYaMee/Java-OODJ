1.0	COURSEWORK TITLE

PRELOVED HOUSEHOLD ITEMS TRADING SYSTEM

2.0	THE COURSEWORK OVERVIEW

The main purpose of this system is to simulate an online buying and selling payment system. You are required to develop a system that allows registered users to buy and sell preloved (used) household items (eg. Chair, table, television, cupboard, bed, etc.). The registered users may also use the system to view their transactions and to update their personal details. A registered user who wishes to sell items must have sufficient credit in their account (minimum of RM10) so that in the event of a successful sale, a success fee will be deducted from the seller’s account for each item that was sold for at least RM5. In the event of a sale, items that cost between RM5 – RM100 will incur a 5% success fee, items that cost between RM101 and RM1000 will incur a 10% success fee, and items that cost more than RM1000 will incur a 15% success fee. No success fee is deducted from items that are sold for less than RM5. A registered user who wishes to purchase an item may do so by paying with their credit card or via online banking. When a sale is made, the buyer’s “trusted buyer” rating will increase by 1 after the buyer successfully makes the payment for the item that were purchased. You may add your own unique method of rating a “trusted seller”. 

3.0	OBJECTIVES OF THIS COURSEWORK
-	Develop the practical ability to describe, justify, and implement an object oriented system.

4.0	LEARNING OUTCOMES
At the end of this coursework, you should be able to:
-	Understand the techniques of Object Oriented design
-	Develop hands-on programming skill in developing java applications
-	Design and implement Object Oriented software systems
-	Select appropriate Object Oriented techniques to solve software development problems


5.0	TYPE
-	Individual Assignment

6.0	COURSEWORK DESCRIPTION
-	There are 3 types of registered users: 
    -	 Buyers will use the system to
        -	~~view all preloved items that are on sale or view a certain category of items (eg. Air-cond)~~
        -	~~purchase preloved item(s)~~ 
        -	~~view their buying records~~
        -	~~update their account details: their password, name, contact number, postal address, and e-mail address (you may add more).~~
    -	Sellers will use the system to
        -	~~list preloved items for sale (this may include a title of the item for sale, a description of the item (that may include a picture), the cost of the item, method of delivery/pickup of item, and any other information that you think is necessary~~
        -	~~view preloved items that were purchased and the success fees that was deducted~~
        -	~~view preloved items that have been listed~~
        -	~~update their account details: their password, name, contact number, postal address, and e-mail address~~
        -	~~add credit into their success fee account~~
    -	Administrator(s) will use the system to
        -	~~update their account details~~
        -	~~view all transactions (all items on sale and all items that were purchased).~~
        -	~~view the success fees.~~
	
- All the accounts details are predefined and saved in text files and/or binary files and those files will be updated for transactions and changes made.
-	GUIs should be done for all interactions between users and the system.
-	The system should be running continuously unless an exit command is issued.

7.0	GENERAL REQUIREMENTS
-	The system submitted should compile and be executed without errors.
-	Validation should be done for each entry from the users in order to avoid logical errors. 
-	The implementation code must highlight the use of object oriented programming concepts as required by the solution.

8.0	DELIVERABLES:
-	Preloved household items trading system with complete code submitted in the form of a CD-ROM.
-	Documents delivered in printed and softcopy form.
-	Submission deadline: September 19’th 2016, 7:00 PM

8.1	PRELOVED HOUSEHOLD ITEMS TRADING SYSTEM:
	The completed application of the system as well as the softcopy of the report must be burned onto a CD-ROM. 
	The application must contain all the relevant source code and text files and/or binary files. 

8.2	DOCUMENTS: COURSEWORK REPORT

As part of the assessment, you must submit the project report in printed and softcopy form. The font size used in the report must be 12pt and the font is Times New Roman. Full source code is not allowed to be included in the report. The report must be typed and clearly printed. Besides, the report should have the following format: 

A)	Cover Page: 

All reports must be prepared with a front cover. A protective transparent plastic sheet can be placed in front of the report to protect the front cover. The front cover should be presented with the following details:
-	Module
-	Coursework Title
-	Intake
-	Student name and id
-	Date Assigned (the date the report was handed out)
-	Date Completed (the date the report is due to be handed in)

B)	Table of contents

C)	Contents: 
-	Description and justification of the system model design and the implementation code which illustrate the object oriented programming concepts incorporated into the solution.
-	Additional features which have been incorporated into the solution.
-	Sample outputs when the system is executed with some explanation of the outputs/sections of the code
-	Assumptions

D)	Conclusion 

E)	References