/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tradingsystem;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TomYaMee
 */
public abstract class AccountManagement {
    private String Username;
    private String Password;
    private String Email;
    private String FirstName;
    private String LastName;
    private String AddressLine1;
    private String AddressLine2;
    private int Postcode;
    private String State;
    private String AccountType;
    private int Rating;   
    public boolean found;
    private String ContactNo;
    
    Function func = new Function();
    
    //Constructor
    public AccountManagement(){
        
    }
    
    //Get Methods
    public String getUsername(){
        return this.Username;
    }
    
    public String getPassword(){
        return this.Password;
    }
    
    public String getEmail(){
        return this.Email;
    }
    
    public String getFirstname(){
        return this.FirstName;
    }
    
    public String getLastname(){
        return this.LastName;
    }
    
    public String getAddressLine(){
        return this.AddressLine1;
    }
    
    //Overload
    public String getAddressLine(boolean a){
        if (a == true){
            return this.AddressLine2;
        }
        else{
            return getAddressLine();
        }
    }
    
    public int getPostcode(){
        return this.Postcode;
    }
    
    public String getState(){
        return this.State;
    }
    
    public String getAccountType(){
        return this.AccountType;
    }
    
    public String getContactNo(){
        return this.ContactNo;
    }
    
    public int getRating(){
        return this.Rating;
    }    
    
    //Set Methods
    public void setUsername(String name){
        this.Username = name;
    }
    
    public void setPassword(String pass){
        this.Password = pass;
    }
    
    public void setEmail(String email){
        this.Email = email;
    }
    
    public void setFirstname(String firstname){
        this.FirstName = firstname;
    }
    
    public void setLastname(String lastname){
        this.LastName = lastname;
    }
    
    public void setAddressline(String address){
        this.AddressLine1 = address;
    }
    
    //Overload
    public void setAddressline(String address, boolean x){
        if (x == true){
            this.AddressLine2 = address;
        }
        else{
            setAddressline(address);
        }
    }
    
    //Overload
    public void setAddressline(String address1, String address2){
        this.AddressLine1 = address1;
        this.AddressLine2 = address2;
    }
    
    public void setPostcode(int postcode){
        this.Postcode = postcode;
    }
    
    public void setState(String state){
        this.State = state;
    }
    
    public void setAccountType(String accounttype){
        this.AccountType = accounttype;
    }
    
    public void setContactNo(String contact){
        this.ContactNo = contact;
    }
    
    public void setRating(int rating){
        this.Rating = rating;
    }    
    //Retrieve Session method
    public void getSession(){
        try{
            BufferedReader get = new BufferedReader(new FileReader("Session.txt"));
            String read;
            while ((read = get.readLine()) != null) {
                this.getData(read);
                
            }  
        }
        catch (FileNotFoundException fe){
            try {
             FileWriter writer = new FileWriter("Session.txt");
            }
            catch (IOException IOe){
                func.msgbox(IOe.toString());
            }
        }
        catch(Exception e){
            func.msgbox(e.toString());
        }                    
    } 
    
    //Retrieve data from account method
    public abstract void getData(String username);
    
    //Updated method
    //http://java.happycodings.com/core-java/code69.html
    public abstract void Update();

    //Overload
    public abstract void Update(String Username, String Password, String Email, String FirstName, String LastName, String ContactNo, String AddressLine1, String AddressLine2, int Postcode, String State);

    //Overloading
    //Register method for buyer
    public abstract void Register(String Username, String Password, String Email, String FirstName, String LastName, String ContactNo, String AddressLine1, String AddressLine2, int Postcode, String State);
    
}

class Buyer extends AccountManagement {
    //Constructor
    public Buyer(){
        
    }
    
    @Override
    public void getData(String username){
       try{
            BufferedReader get = new BufferedReader(new FileReader("Account.txt"));
            String read;
            boolean checked = false;
            while (checked == false){
                while ((read = get.readLine()) != null) {
                    String[] splited = read.split("!!");
                    if (splited[0].equals(username)){
                        setUsername(splited[0]);
                        setPassword(splited[1]);
                        setEmail(splited[2]);
                        setFirstname(splited[3]);
                        setLastname(splited[4]);
                        setContactNo(splited[5]);
                        setAddressline(splited[6], splited[7]);
                        setPostcode(Integer.parseInt(splited[8]));
                        setState(splited[9]);
                        setAccountType(splited[10]);
                        //setCredit(Float.parseFloat(splited[11]));
                        if (splited[10].equals("Buyer")){
                        setRating(Integer.parseInt(splited[11]));
                        }
                        
                        this.found = true;
                    }
                 }
                checked = true;
            }  
            if (this.found == false){
                func.msgbox("Username not found!");
            }
        }
        catch (FileNotFoundException fe){
            try {
             FileWriter writer = new FileWriter("Account.txt");
             writer.close();
            }
            catch (IOException IOe){
                func.msgbox(IOe.toString());
            }
        }       
        catch(Exception e){
            func.msgbox(e.toString());
        }    
    }   
    
    @Override
    public void Update(){
        String toupdate;
            toupdate = this.getUsername() + "!!" + this.getPassword() + "!!" + this.getEmail() + "!!" + this.getFirstname() + "!!" + this.getLastname() + "!!" + this.getContactNo() + "!!" + this.getAddressLine() + "!!" + this.getAddressLine(true) + "!!" + this.getPostcode() + "!!" + this.getState() + "!!" + this.getAccountType() + "!!" + this.getRating();
        //}
        try{
            BufferedReader get = new BufferedReader(new FileReader("Account.txt"));
            String read;
            String total = "";
            String old = "";
            while ((read = get.readLine()) != null) {
                total += read + "\r\n";
                String[] splited = read.split("!!");
                if (splited[0].equals(this.getUsername())){
                    old = read;
                }                 
            }
            total = total.replaceAll(old, toupdate);
            
            try{
                FileWriter fw = new FileWriter("Account.txt");
                fw.write(total);
                fw.close();
                }     
            catch(IOException IOe){
                func.msgbox(IOe.toString());
            }
            
        }
        catch(Exception e){
            func.msgbox(e.toString());
        }        
    }  
    
    @Override
    public void Update(String Username, String Password, String Email, String FirstName, String LastName, String ContactNo, String AddressLine1, String AddressLine2, int Postcode, String State){
        String toupdate;
            toupdate = Username + "!!" + Password + "!!" + Email + "!!" + FirstName + "!!" + LastName + "!!" + ContactNo + "!!" + AddressLine1 + "!!" + AddressLine2 + "!!" + Postcode + "!!" + State + "!!" + this.getAccountType() + "!!" + this.getRating();
        try{
            BufferedReader get = new BufferedReader(new FileReader("Account.txt"));
            String read;
            String total = "";
            String old = "";
            while ((read = get.readLine()) != null) {
                total += read + "\r\n";
                String[] splited = read.split("!!");
                if (splited[0].equals(Username)){
                    old = read;
                }                 
            }
            total = total.replaceAll(old, toupdate);
            
            try{
                FileWriter fw = new FileWriter("Account.txt");
                fw.write(total);
                fw.close();
                }     
            catch(IOException IOe){
                func.msgbox(IOe.toString());
            }
            
        }
        catch(Exception e){
            func.msgbox(e.toString());
        }        
    }  
    
    @Override
    public void Register(String Username, String Password, String Email, String FirstName, String LastName, String ContactNo, String AddressLine1, String AddressLine2, int Postcode, String State){
        String tosave = Username + "!!" + Password + "!!" + Email + "!!" + FirstName + "!!" + LastName + "!!" + ContactNo + "!!" + AddressLine1 + "!!" + AddressLine2 + "!!" + Postcode + "!!" + State + "!!Buyer!!" + this.getRating();
        // http://www.codejava.net/java-se/file-io/how-to-read-and-write-text-file-in-java
        try{
            FileWriter writer = new FileWriter("Account.txt", true);
            //http://stackoverflow.com/questions/10852856/inserting-new-lines-when-writing-to-a-text-file-in-java
            writer.write(tosave + "\n");
            func.msgbox("Account successfully created!\nUsername: " + Username + "\nEmail: " + Email + "\nAccount Type: Buyer" );
            writer.close();
        }
        catch(IOException e){
            
        }
    }    
}

class Seller extends AccountManagement {
    private double Credit;

    //Constructor
    public Seller(){
        
    }
    
    public double getCredit(){
        return this.Credit;
    }
    
    public void setCredit(double credit){
        this.Credit = credit;
    }

    
    //Register method for Seller
    @Override
    public void Register(String Username, String Password, String Email, String FirstName, String LastName, String ContactNo, String AddressLine1, String AddressLine2, int Postcode, String State){
        Credit = Math.round(Credit * 100.0) / 100.0;
        String tosave = Username + "!!" + Password + "!!" + Email + "!!" + FirstName + "!!" + LastName + "!!" + ContactNo + "!!" + AddressLine1 + "!!" + AddressLine2 + "!!" + Postcode + "!!" + State + "!!Seller!!" + this.getRating() + "!!" + this.Credit;
        // http://www.codejava.net/java-se/file-io/how-to-read-and-write-text-file-in-java
        try{
            FileWriter writer = new FileWriter("Account.txt", true);
            //http://stackoverflow.com/questions/10852856/inserting-new-lines-when-writing-to-a-text-file-in-java
            writer.write(tosave + "\n");
            func.msgbox("Account successfully created!\nUsername: " + Username + "\nEmail: " + Email + "\nAccount Type: Seller" );
            writer.close();
        }
        
        catch(IOException e){
            
        }
    }  
    
    @Override
    public void getData(String username){
       try{
            BufferedReader get = new BufferedReader(new FileReader("Account.txt"));
            String read;
            boolean checked = false;
            while (checked == false){
                while ((read = get.readLine()) != null) {
                    String[] splited = read.split("!!");
                    if (splited[0].equals(username)){
                        setUsername(splited[0]);
                        setPassword(splited[1]);
                        setEmail(splited[2]);
                        setFirstname(splited[3]);
                        setLastname(splited[4]);
                        setContactNo(splited[5]);
                        setAddressline(splited[6], splited[7]);
                        setPostcode(Integer.parseInt(splited[8]));
                        setState(splited[9]);
                        setAccountType(splited[10]);
                        setRating(Integer.parseInt(splited[11]));
                        setCredit(Float.parseFloat(splited[12]));
                        this.found = true;
                    }
                 }
                checked = true;
            }  
            if (this.found == false){
                func.msgbox("Username not found!");
            }
        }
        catch (FileNotFoundException fe){
            try {
             FileWriter writer = new FileWriter("Account.txt");
            }
            catch (IOException IOe){
                func.msgbox(IOe.toString());
            }
        }       
        catch(Exception e){
            func.msgbox(e.toString());
        }    
    }    
    
    @Override
    public void Update(){
        String toupdate;
            toupdate = this.getUsername() + "!!" + this.getPassword() + "!!" + this.getEmail() + "!!" + this.getFirstname() + "!!" + this.getLastname() + "!!" + this.getContactNo() + "!!" + this.getAddressLine() + "!!" + this.getAddressLine(true) + "!!" + this.getPostcode() + "!!" + this.getState() + "!!" + this.getAccountType() +"!!" + this.getRating() + "!!" + this.getCredit();
        try{
            BufferedReader get = new BufferedReader(new FileReader("Account.txt"));
            String read;
            String total = "";
            String old = "";
            while ((read = get.readLine()) != null) {
                total += read + "\r\n";
                String[] splited = read.split("!!");
                if (splited[0].equals(this.getUsername())){
                    old = read;
                }                 
            }
            total = total.replaceAll(old, toupdate);
            
            try{
                FileWriter fw = new FileWriter("Account.txt");
                fw.write(total);
                fw.close();
                }     
            catch(IOException IOe){
                func.msgbox(IOe.toString());
            }
            
        }
        catch(Exception e){
            func.msgbox(e.toString());
        }        
    }    
    
    @Override
    public void Update(String Username, String Password, String Email, String FirstName, String LastName, String ContactNo, String AddressLine1, String AddressLine2, int Postcode, String State){
        String toupdate;
            toupdate = Username + "!!" + Password + "!!" + Email + "!!" + FirstName + "!!" + LastName + "!!" + ContactNo + "!!" + AddressLine1 + "!!" + AddressLine2 + "!!" + Postcode + "!!" + State + "!!" + this.getAccountType() +"!!" + this.getRating() + "!!" + this.getCredit();
        try{
            BufferedReader get = new BufferedReader(new FileReader("Account.txt"));
            String read;
            String total = "";
            String old = "";
            while ((read = get.readLine()) != null) {
                total += read + "\r\n";
                String[] splited = read.split("!!");
                if (splited[0].equals(Username)){
                    old = read;
                }                 
            }
            total = total.replaceAll(old, toupdate);
            
            try{
                FileWriter fw = new FileWriter("Account.txt");
                fw.write(total);
                fw.close();
                }     
            catch(IOException IOe){
                func.msgbox(IOe.toString());
            }
            
        }
        catch(Exception e){
            func.msgbox(e.toString());
        }        
    }
        
}

class Admin extends AccountManagement {    
    //Constructor
    public Admin(){
        
    }
    
    
    //Register method for admin
    @Override
    public void Register(String Username, String Password, String Email, String FirstName, String LastName, String ContactNo, String AddressLine1, String AddressLine2, int Postcode, String State){
        String tosave = Username + "!!" + Password + "!!" + Email + "!!" + FirstName + "!!" + LastName + "!!" + ContactNo + "!!" + AddressLine1 + "!!" + AddressLine2 + "!!" + Postcode + "!!" + State + "!!Admin";
        // http://www.codejava.net/java-se/file-io/how-to-read-and-write-text-file-in-java
        try{
            FileWriter writer = new FileWriter("Account.txt", true);
            //http://stackoverflow.com/questions/10852856/inserting-new-lines-when-writing-to-a-text-file-in-java
            writer.write(tosave + "\n");
            func.msgbox("Account successfully created!\nUsername: " + Username + "\nEmail: " + Email + "\nAccount Type: Admin" );
            writer.close();
        }
        catch(IOException e){
            
        }       
    }
    
    @Override
    public void Update(){
        String toupdate;
            toupdate = this.getUsername() + "!!" + this.getPassword() + "!!" + this.getEmail() + "!!" + this.getFirstname() + "!!" + this.getLastname() + "!!" + this.getContactNo() + "!!" + this.getAddressLine() + "!!" + this.getAddressLine(true) + "!!" + this.getPostcode() + "!!" + this.getState() + "!!" + this.getAccountType();
        try{
            BufferedReader get = new BufferedReader(new FileReader("Account.txt"));
            String read;
            String total = "";
            String old = "";
            while ((read = get.readLine()) != null) {
                total += read + "\r\n";
                String[] splited = read.split("!!");
                if (splited[0].equals(this.getUsername())){
                    old = read;
                }                 
            }
            total = total.replaceAll(old, toupdate);
            
            try{
                FileWriter fw = new FileWriter("Account.txt");
                fw.write(total);
                fw.close();
                }     
            catch(IOException IOe){
                func.msgbox(IOe.toString());
            }
            
        }
        catch(Exception e){
            func.msgbox(e.toString());
        }        
    }
    
    @Override
    public void Update(String Username, String Password, String Email, String FirstName, String LastName, String ContactNo, String AddressLine1, String AddressLine2, int Postcode, String State){
        String toupdate;
            toupdate = Username + "!!" + Password + "!!" + Email + "!!" + FirstName + "!!" + LastName + "!!" + ContactNo + "!!" + AddressLine1 + "!!" + AddressLine2 + "!!" + Postcode + "!!" + State + "!!" + this.getAccountType();
        try{
            BufferedReader get = new BufferedReader(new FileReader("Account.txt"));
            String read;
            String total = "";
            String old = "";
            while ((read = get.readLine()) != null) {
                total += read + "\r\n";
                String[] splited = read.split("!!");
                if (splited[0].equals(Username)){
                    old = read;
                }                 
            }
            total = total.replaceAll(old, toupdate);
            
            try{
                FileWriter fw = new FileWriter("Account.txt");
                fw.write(total);
                fw.close();
                }     
            catch(IOException IOe){
                func.msgbox(IOe.toString());
            }
            
        }
        catch(Exception e){
            func.msgbox(e.toString());
        }        
    }

    @Override
    public void getData(String username){
       try{
            BufferedReader get = new BufferedReader(new FileReader("Account.txt"));
            String read;
            boolean checked = false;
            while (checked == false){
                while ((read = get.readLine()) != null) {
                    String[] splited = read.split("!!");
                    if (splited[0].equals(username)){
                        setUsername(splited[0]);
                        setPassword(splited[1]);
                        setEmail(splited[2]);
                        setFirstname(splited[3]);
                        setLastname(splited[4]);
                        setContactNo(splited[5]);
                        setAddressline(splited[6], splited[7]);
                        setPostcode(Integer.parseInt(splited[8]));
                        setState(splited[9]);
                        setAccountType(splited[10]);
                        //setCredit(Float.parseFloat(splited[11]));
                        //setRating(Integer.parseInt(splited[11]));
                        
                        this.found = true;
                    }
                 }
                checked = true;
            }  
            if (this.found == false){
                func.msgbox("Username not found!");
            }
        }
        catch (FileNotFoundException fe){
            try {
             FileWriter writer = new FileWriter("Account.txt");
            }
            catch (IOException IOe){
                func.msgbox(IOe.toString());
            }
        }       
        catch(Exception e){
            func.msgbox(e.toString());
        }    
    }  
    
    public List<String> getUserlist(){
        List<String> total = new ArrayList<String>();
        try{
            BufferedReader get = new BufferedReader(new FileReader("Account.txt"));
            String read;
            
            while ((read = get.readLine()) != null) {
                String[] splited = read.split("!!");
                total.add(splited[0]);
                total.add(splited[2]);
                total.add(splited[3]);
                total.add(splited[4]);
                total.add(splited[5]);
                total.add(splited[6]);       
                total.add(splited[7]);  
                total.add(splited[8]);  
                total.add(splited[9]);  
                total.add(splited[10]);                  
            }
            
            get.close();
            
        }
        catch(FileNotFoundException fe){
            try {
             FileWriter writer = new FileWriter("Account.txt");
             writer.close();
            }
            catch (IOException IOe){
                func.msgbox(IOe.toString());
            }
        }
        catch(Exception e){
            func.msgbox(e.toString());
        }   
        return total;        
    }             
}
