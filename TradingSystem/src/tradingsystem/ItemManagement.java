/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tradingsystem;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TomYaMee
 */
public class ItemManagement{
    private String ItemName;
    private String Description;
    private float Cost;
    private String Method;
    private String Category;
    private String Owner;
    private int ID;
    
    Function func = new Function();
    
    //Constructor
    public ItemManagement(){
        
    }
    
    //Get Method
    public String getItemName(){
        return this.ItemName;
    }
    
    public String getDescription(){
        return this.Description;
    }
    
    public float getCost(){
        return this.Cost;
    }
    
    public String getMethod(){
        return this.Method;
    }
    
    public String getCategory(){
        return this.Category;
    }
    
    public String getOwner(){
        return this.Owner;
    }
    
    public int getID(){
        return this.ID;
    }
    
    //Set Method
    public void setItemName(String name){
        this.ItemName = name;
    }
    
    public void setDescription(String desc){
        this.Description = desc;
    }
    
    public void setCost(float cost){
        this.Cost = cost;
    }
    
    public void setMethod(String method){
        this.Method = method;
    }
    
    public void setCategory(String cat){
        this.Category = cat;
    }
    
    public void setOwner(String owner){
        this.Owner = owner;
    }
    
    public void setID(int id){
        this.ID = id;
    }
    
    //Get data
    public void getData(String ID){
       try{
            BufferedReader get = new BufferedReader(new FileReader("Items.txt"));
            String read;
            boolean checked = false;
            while (checked == false){
                while ((read = get.readLine()) != null) {
                    String[] splited = read.split("!!");
                    if (splited[0].equals(ID)){
                        setItemName(splited[2]);
                        setDescription(splited[3]);
                        setOwner(splited[1]);
                        setCost(Float.parseFloat(splited[4]));
                        setMethod(splited[5]);
                        setCategory(splited[6]);
                    }
                 }
                checked = true;
            }  
        }
        catch (FileNotFoundException fe){
            try {
             FileWriter writer = new FileWriter("Items.txt");
            }
            catch (IOException IOe){
                func.msgbox(IOe.toString());
            }
        }       
        catch(Exception e){
            func.msgbox(e.toString());
        }           
    }
    
    //Add new item
    public void AddItem(String Owner, String ItemName, String Description, double Cost, String Method, String Category){
        String tosave = Owner + "!!" + ItemName + "!!" + Description + "!!" + Cost + "!!" + Method + "!!" + Category;
        int lines = 1;
        try{
            BufferedReader get = new BufferedReader(new FileReader("Items.txt"));
            while (get.readLine() != null) {
                lines++;
            }
            get.close();
        }
        catch(FileNotFoundException fe){
            try{
                FileWriter writer = new FileWriter("Items.txt", true);
                writer.close();
            }
            catch(IOException ioe){
                
            }
        }
        catch(IOException ioe2){
        
        }
        
        tosave = lines + "!!" + tosave;
        
        // http://www.codejava.net/java-se/file-io/how-to-read-and-write-text-file-in-java
        try{
            FileWriter writer = new FileWriter("Items.txt", true);
            //http://stackoverflow.com/questions/10852856/inserting-new-lines-when-writing-to-a-text-file-in-java
            writer.write(tosave + "\n");
            func.msgbox("Item succesfully added! \nItem Name: " + ItemName + "\nDescription: " + Description + "\nCost: " + Cost + "\nCategory: " + Category);
            writer.close();
        }
        
        catch(IOException e){
            
        }
    }
    //Get items
    public List<String> getItems(String Category){
        List<String> total = new ArrayList<String>();
        try{
            BufferedReader get = new BufferedReader(new FileReader("Items.txt"));
            String read;
            
            while ((read = get.readLine()) != null) {
                String[] splited = read.split("!!");
                if (Category.equals("All")){
                    total.add(splited[0]);
                    total.add(splited[2]);
                    total.add(splited[4]);
                    total.add(splited[5]);
                    total.add(splited[6]);  
                }
                else if (Category.equals(splited[6])){
                    total.add(splited[0]);
                    total.add(splited[2]);
                    total.add(splited[4]);
                    total.add(splited[5]);
                    total.add(splited[6]);                 
                }
            }
            
            get.close();
            
        }
        catch(FileNotFoundException fe){
            try {
             FileWriter writer = new FileWriter("Items.txt");
             writer.close();
            }
            catch (IOException IOe){
                func.msgbox(IOe.toString());
            }
        }
        catch(Exception e){
            func.msgbox(e.toString());
        }   
        return total;
    }        
    
    //Overloading
    public List<String> getItems(boolean a){
        //http://stackoverflow.com/questions/14518195/how-can-i-add-new-item-to-the-string-array
        List<String> total = new ArrayList<String>();
        try{
            BufferedReader get = new BufferedReader(new FileReader("Items.txt"));
            String read;
            
            while ((read = get.readLine()) != null) {
                String[] splited = read.split("!!");
                if (a == true){
                    if (splited[1].equals(this.Owner)){
                        total.add(splited[0]);
                        total.add(splited[2]);
                        total.add(splited[4]);
                        total.add(splited[5]);
                        total.add(splited[6]);
                    }         
                }
                else{
                        total.add(splited[0]);
                        total.add(splited[2]);
                        total.add(splited[4]);
                        total.add(splited[5]);
                        total.add(splited[6]);                    
                }
            }
            
            get.close();
            
        }
        catch(FileNotFoundException fe){
            try {
             FileWriter writer = new FileWriter("Items.txt");
             writer.close();
            }
            catch (IOException IOe){
                func.msgbox(IOe.toString());
            }
        }
        catch(Exception e){
            func.msgbox(e.toString());
        }   
        return total;
    }
    
    //Get Transaction List
    public List<String> getTransaction(){
        Buyer acc = new Buyer();
        acc.getSession();
        List<String> total = new ArrayList<String>();
        try{
            BufferedReader get = new BufferedReader(new FileReader("BuyingLog.txt"));
            String read;
            
            while ((read = get.readLine()) != null) {
                String[] splited = read.split("!!");
                if (splited[2].equals(acc.getUsername())){
                    total.add(splited[0]);
                    total.add(splited[1]);
                    total.add(splited[2]);
                    total.add(splited[3]);
                    total.add(splited[4]);
                    total.add(splited[5]);
                    total.add(splited[6]);
                }                 
            }
            
            get.close();
            
        }
        catch(FileNotFoundException fe){
            try {
             FileWriter writer = new FileWriter("BuyingLog.txt");
             writer.close();
            }
            catch (IOException IOe){
                func.msgbox(IOe.toString());
            }
        }
        catch(Exception e){
            func.msgbox(e.toString());
        }   
        return total;        
    }    
    
    //Overload
    public List<String> getTransaction(String Username){
        List<String> total = new ArrayList<String>();
        try{
            BufferedReader get = new BufferedReader(new FileReader("BuyingLog.txt"));
            String read;
            
            while ((read = get.readLine()) != null) {
                String[] splited = read.split("!!");
                if (splited[3].equals(Username)){
                    total.add(splited[0]);
                    total.add(splited[1]);
                    total.add(splited[2]);
                    total.add(splited[3]);
                    total.add(splited[4]);
                    total.add(splited[5]);
                    total.add(splited[6]);
                }                 
            }
            
            get.close();
            
        }
        catch(FileNotFoundException fe){
            try {
             FileWriter writer = new FileWriter("BuyingLog.txt");
             writer.close();
            }
            catch (IOException IOe){
                func.msgbox(IOe.toString());
            }
        }
        catch(Exception e){
            func.msgbox(e.toString());
        }   
        return total;        
    }
    
    //Add buying record
    public void addRecord(String ID, String Owner, String Username, float Cost, String Method){
        int lines = 1;
        try{
            BufferedReader get = new BufferedReader(new FileReader("BuyingLog.txt"));
            while (get.readLine() != null) {
                lines++;
            }
            get.close();
        }
        catch(FileNotFoundException fe){
            try{
                FileWriter writer = new FileWriter("BuyingLog.txt", true);
                writer.close();
            }
            catch(IOException ioe){

            }
        }
        catch(IOException ioe2){

        }

        Seller owner = new Seller();
        owner.getData(Owner);
        Buyer buyer = new Buyer();
        buyer.getData(Username);
        double tocredit = 0;
        if (Cost > 1000){
            tocredit = (Cost * 15 / 100);
        }
        else if (Cost > 100){
            tocredit = (Cost * 10 / 100);
        }
        else if (Cost >= 5){
            tocredit = (Cost * 5 / 100);
        }
        tocredit = Math.round(tocredit * 100.0 )/ 100.0;
        double oldcredit = Math.round(owner.getCredit() * 100.0 )/ 100.0;
        double credit = (oldcredit - tocredit);
        owner.setCredit(credit);
        int rating = (owner.getRating() + 1);
        owner.setRating(rating);
        int rating2 = (buyer.getRating() + 1);
        buyer.setRating(rating2);
        
        owner.Update();
        buyer.Update();
        
        String tosave = lines + "!!" + ID + "!!" + Owner + "!!" + Username + "!!" + Cost + "!!" + tocredit + "!!" + Method;
        
        try{
            FileWriter writer = new FileWriter("BuyingLog.txt", true);
            //http://stackoverflow.com/questions/10852856/inserting-new-lines-when-writing-to-a-text-file-in-java
            writer.write(tosave + "\n");
            func.msgbox("Payment Made!");
            writer.close();
        }

        catch(IOException e){

        }  
        

    }
}

class Admin2 extends ItemManagement{
    //Constructor
    public Admin2(){
        
    }
    
    //Get Transaction List
    @Override
    public List<String> getTransaction(){
        List<String> total = new ArrayList<String>();
        try{
            BufferedReader get = new BufferedReader(new FileReader("BuyingLog.txt"));
            String read;
            
            while ((read = get.readLine()) != null) {
                String[] splited = read.split("!!");
                total.add(splited[0]);
                total.add(splited[1]);
                total.add(splited[2]);
                total.add(splited[3]);
                total.add(splited[4]);
                total.add(splited[5]);
                total.add(splited[6]);                
            }
            
            get.close();
            
        }
        catch(FileNotFoundException fe){
            try {
             FileWriter writer = new FileWriter("BuyingLog.txt");
             writer.close();
            }
            catch (IOException IOe){
                func.msgbox(IOe.toString());
            }
        }
        catch(Exception e){
            func.msgbox(e.toString());
        }   
        return total;        
    }     
}
